This is an asynchronous retrospective for Pipeline Authoring {Insert iteration name and YYYY-MM-DD date range}. :tada: 

This issue is private (confidential) to the Pipeline Authoring group, plus anyone else
who worked with the group during Iteration 3, to ensure everyone feels
comfortable sharing freely. 

Please look back at your experiences working on this iteration, ask yourself
**:star2: What praise do you have for the group?**,
**:+1: Did the team meet the iteration goals?**, **:-1: What didn’t go well for this iteration and why?**, and **:chart_with_upwards_trend: what action items can we have as takeaways to improve on going
forward?**, and honestly describe your thoughts and feelings below.

For each point you want to raise, please create a **new discussion** with the
relevant emoji, so that others can weigh in with their perspectives, and so that
we can easily discuss any follow-up action items in-line.

If there is anything you are not comfortable sharing here, please message your
manager directly. Note, however, that 'Emotions are not only allowed in
retrospectives, they should be encouraged', so we'd love to hear from you here
if possible.
